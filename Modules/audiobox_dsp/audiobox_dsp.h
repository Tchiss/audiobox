/*******************************************************************************
 The block below describes the properties of this module, and is read by
 the Projucer to automatically generate project code that uses it.
 For details about the syntax and how to create or use a module, see the
 JUCE Module Format.md file.


 BEGIN_JUCE_MODULE_DECLARATION

  ID:               audiobox_dsp
  vendor:           Mathis Raibaud
  version:          1.0.0
  name:             Audiobox DSP classes
  description:      Shared DSP classes
  dependencies:     juce_dsp

 END_JUCE_MODULE_DECLARATION

*******************************************************************************/

#pragma once
#define AUDIOBOX_DSP_H_INCLUDED

#include <juce_dsp/juce_dsp.h>

#include "interfaces/audiobox_AudioProcessor.h"
#include "effects/audiobox_AutoPan.h"
#include "oscillator/audiobox_Oscillator.h"