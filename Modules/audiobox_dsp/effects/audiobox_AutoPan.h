//
// Created by Mathis Raibaud on 22/01/2022.
//

namespace audiobox
{
namespace dsp
{

/** An audio stereo panner automatically modulated by a LFO */
class AutoPan : public AudioProcessor
{
public:

    AutoPan();

    void prepare(const juce::dsp::ProcessSpec& spec);

    void process(const juce::AudioBuffer<float> &inputBuffer,
                 juce::AudioBuffer<float> &outputBuffer,
                 int numSamples,
                 bool additive) override;

    void setAmount(float amount);
    void setFrequency(float frequency);
    void setPhaseOffset(float offset);

    void reset() override;

private:

    double sampleRate{};
    std::array<juce::dsp::Oscillator<float>, 2> lfos;
    std::array<juce::dsp::Gain<float>, 2> gains;
    juce::SmoothedValue<float> amount;
    juce::SmoothedValue<float> offset;

};

}
}


