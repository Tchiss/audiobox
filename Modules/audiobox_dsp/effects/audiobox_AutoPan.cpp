//
// Created by Mathis Raibaud on 22/01/2022.
//

/*
  ==============================================================================

    AutoPan.cpp
    Created: 22 Nov 2020 3:07:59pm
    Author:  Mathis Raibaud

  ==============================================================================
*/

audiobox::dsp::AutoPan::AutoPan() :
        sampleRate(0)
{
    offset.setCurrentAndTargetValue(180.f);
    lfos[0].initialise([] (float x) { return -0.5f + 0.5f * std::sin(x) ; } );
    lfos[1].initialise([&] (float x) { return -0.5f + 0.5f * std::sin(x + (offset.getNextValue() * juce::MathConstants<float>::pi / 180.f)) ; } );
    lfos[0].setFrequency(2.f);
    lfos[1].setFrequency(2.f);
}

void audiobox::dsp::AutoPan::prepare(const juce::dsp::ProcessSpec& spec)
{
    sampleRate = spec.sampleRate;

    for (size_t i = 0; i < 2; ++i)
    {
        lfos[i].prepare(spec);
        gains[i].prepare(spec);
    }

    amount.setCurrentAndTargetValue(1.f);
    amount.reset(sampleRate, 0.05);
    offset.reset(sampleRate, 0.05);
}

void audiobox::dsp::AutoPan::process(const juce::AudioBuffer<float> &inputBuffer,
                                              juce::AudioBuffer<float> &outputBuffer,
                                              int numSamples,
                                              bool additive)
{
    jassert (inputBuffer.getNumChannels() == outputBuffer.getNumChannels());
    jassert (inputBuffer.getNumChannels() == 2);
    jassert (inputBuffer.getNumSamples()  >= numSamples);

    for (size_t channel = 0; channel < inputBuffer.getNumChannels(); ++channel)
    {
        auto* inputSamples = inputBuffer.getReadPointer(channel);
        auto* outputSamples = outputBuffer.getWritePointer(channel);

        auto& lfo = lfos[channel];
        auto& gain = gains[channel];

        for (size_t i = 0; i < numSamples; ++i)
        {
            auto lfoValue = amount.getNextValue() * lfo.processSample(0);
            gain.setGainLinear(1.f + lfoValue);
//            auto gainDecibels = juce::jmap(1.f + lfoValue, 0.f, 1.f, -30.f, 0.f);
//            gain.setGainDecibels(gainDecibels);

            outputSamples[i] = gain.processSample(inputSamples[i]);
        }
    }
}

void audiobox::dsp::AutoPan::setFrequency(float frequency)
{
    jassert(juce::isPositiveAndBelow(frequency, sampleRate));

    for (auto& lfo : lfos)
        lfo.setFrequency(frequency);
}

void audiobox::dsp::AutoPan::setAmount(float amount)
{
    this->amount.setTargetValue(amount);
}

void audiobox::dsp::AutoPan::setPhaseOffset(float offset)
{
    this->offset.setTargetValue(offset);
}

void audiobox::dsp::AutoPan::reset()
{
    for (auto& lfo : lfos)
        lfo.reset();

    for (auto& gain : gains)
        gain.reset();
}

