//
// Created by Mathis Raibaud on 22/01/2022.
//

namespace audiobox
{
namespace dsp
{

/** A base class designed for all audio processors that apply processing to input data and write it to output data. */
class AudioProcessor
{
public:

    /** Destructor. */
    virtual ~AudioProcessor() = default;

    /** Processes audio.

        @param inputBuffer The buffer to read from.
        @param outputBuffer The buffers to write the result into.
        @param numSamples The number of samples to process.
        @param additive Specifies whether the existing contents of outputBuffer should be erased by or added to the processed samples.
     */
    virtual void process(const juce::AudioBuffer<float>& inputBuffer,
                         juce::AudioBuffer<float>& outputBuffer,
                         int numSamples,
                         bool additive) = 0;

    /** Allows the processor to reset its memory or to release anything it no longer needs.

        This is typically used when the system is reset or set to pause for a while.
     */
    virtual void reset() = 0;

};


}
}
